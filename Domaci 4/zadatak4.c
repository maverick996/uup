//	4. Unosi se broj n i koordinate rupe x i y.Zatim se unosi n pozicija kugli x1_i, y1_i kao i pravci kretanja 
//	x2_i, y2_i.Smatrati da se kugla kreće po pravoj koja spaja(x1_i, y1_i) i(x2_i, y2_i) (znači po celoj pravoj, 
//	ne samo polupravoj).Ispisati koliko kugli će upasti u rupu. (pojašnjenje: ispisati broj pravih tako da tačka(x, y) 
//	se nalazi na pravoj koja povezuje(x1_i, y1_i) i(x2_i, y2_i)).

#include <stdio.h>

int main() {

	int i, k = 0, n;
	double x, y, x1, x2, y1, y2;
	scanf_s("%d%lf%lf", &n, &x, &y);
	for (i = 1;i <= n;i++) {
		scanf_s("%lf%lf%lf%lf", &x1, &y1, &x2, &y2);
		if (x == x1 && x == x2)
			k++;
		else if (y - y1 == ((y2 - y1) / (x2 - x1)*(x - x1)))
			k++;
	}
	printf("%d\n", k);

	return 0;
}