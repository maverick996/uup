//	6. Data su dva broja n i m. Izračunati binomni koeficijent 

#include <stdio.h>

int main() {

	int i, n,m,fak,r,s;
	double p;

	scanf_s("%d%d", &n, &m);
	r = n;
	s = m;
	for (i = 1;i < m;i++) {
		r = r * (n - i);
		s = s *i;
	}
	p = r / s;
	printf("%.0lf\n", p);

	return 0;
}