//	3.Unosi se broj n. Izračunati:
//	1 / (1 * n) + 1 / (2 * (n - 1)) + 1 / (3 * (n - 2)) + … + 1 / (i*j) sve dok je i <= j

#include <stdio.h>

int main() {

	int i, j, n;
	double p=0;
	scanf_s("%d", &n);
	for (i = 1;i <= n-i+1;i++)
		p = 1.0 / (i*(n-i+1)) + p;
	printf("%lf", p);

	return 0;
}