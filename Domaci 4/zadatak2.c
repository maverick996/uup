//	2. Unosi se početna pozitivna masa M i broj nadolazećih igrača n.Njihove mase se redom učitavaju m1, m2, …, 
//	mn.Trenutno učitanog igrača m_i(u i - tom koraku) možemo ili da pojedemo ili da nas on pojede ili da ga ignorišemo u 
//	zavisnosti kakav je odnos naše i njegove mase.Ako je 60 % naše trenutne mase veće od njegove mase(0.6 * M > m_i) mi ga 
//	jedemo i naša nova masa iznosi M + m_i, ukoliko je naša masa manja od 60 % njegove mase(M < 0.6 * m_i) on nas jede i 
//	završavamo igru.U suprotnom ignorišemo tog igrača.Ako smo preživeli učitajemo masu narednog igrača.Ispisati na ekranu našu konačnu masu, 
//	ako smo preživeli, u suprotnom ispisati da smo izgubili.


#include <stdio.h>

int main() {

	int i, n,c,m,p=0;
	scanf_s("%d%d", &c,&n);
	
	for (i = 1;i <= n;i++) {
		scanf_s("%d", &m);
		if (c * 0.6 > m)
			c = c + m;
		else if (c < (0.6*m)) {
			p = 1;
			break;
		}
	}

	if(p)
		printf("Pojedeni ste\n");
	else
		printf("Konacna masa: %d\n", c);

	return 0;
}