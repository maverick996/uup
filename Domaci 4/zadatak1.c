// 1. Unosi se broj n.Izračunati:
//1 / (1 * 3) + 1 / (3 * 5) + 1 / (5 * 7) + … + 1 / ((2 * n - 1) * (2 * n + 1))

#include <stdio.h>

int main() {

	int i, n;
	double p = 0;
	scanf_s("%d", &n);
	
	for (i = 1;i <= n;i++) {
		p = 1.0 / ((2 * i - 1)*(2 * i + 1)) + p;
	}

	printf("%.10lf\n", p);

	return 0;
}