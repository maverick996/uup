//10. Dati su brojevi n i p. Izračunati najveći stepen k tako da p^k (p stepenovano na k) deli n.

#include <stdio.h>

int main() {
	int n, i=1,j,m,e=1;

	scanf_s("%d%d", &n,&m);
	while (e<n) {
		e = e * m;
		if (n % e == 0)
			j = i;

		i++;
	}
	printf("%d\n", j);

	return 0;
}
