//3.Unosi se broj n. Zatim se unosi n trouglova tako sto im se daju koordinate temena (Ax_i, Ay_i), (Bx_i, By_i), (Cx_i, Cy_i). Izračunati sumu površina svih trouglova.

#include <stdio.h>

int main() {
	float x1, y1, x2, y2, x3, y3, p,n,i,s=0;
	scanf_s("%f", &n);
	for (i = 1;i <= n;i++){
		scanf_s("%f %f %f %f %f %f", &x1, &y1, &x2, &y2, &x3, &y3);
		p = (x1*(y2 - y3) + x2*(y3 - y1) + x3*(y1 - y2)) / 2;
		if (p < 0)
			p = p * -1;
		if (p != 0)
			s = s + p;
		else
			printf("Ovo nije trougao\n");
	}

	
	printf("%f\n", s);
	return 0;
}
