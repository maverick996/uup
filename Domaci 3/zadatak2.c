// 2. Unosi se broj n. Ispisati koliko jedinica ima u binarnom zapisu (tj. u zapisu sa osnovom 2, npr 1010 = 10102)

#include <stdio.h>

int main() {

	int n,c=0;

	scanf_s("%d", &n);

	while (n != 0) {
		if (n % 2 == 1)
			c++;
		n = n / 2;
	}
	printf("%d\n", c);
	return 0;
}