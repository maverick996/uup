//6. Dat je Fibonačijev niz, napravljen tako da je f_0 = 0, f_1 = 1 a svaki naredni član je zbir prethodna dva, tj.f_{ n + 1 } = f_n + f_{ n - 1 }.
//Tako Fibonačijev niz izgleda : 0, 1, 1, 2, 3, 5, 8, 13, ….Dat je broj n, izračunati n - ti član Fibonačijevog niza.

#include <stdio.h>

int main() {
	int i, c, n, b, a;

	scanf_s("%d", &n);
	a = 0;
	b = 1;
	if (n > 1) {
		for (i = 2;i <= n;i++) {
			c = a + b;
			a = b;
			b = c;

		}
	}
	else if (n == 1)
		c = 1;
	else
		c = 0;
	printf("%d ", c);

	return 0;
}
