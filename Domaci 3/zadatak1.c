// 1 Unosi se broj n. Ispisati sumu prvih n prostih brojeva. (Prvi prost broj je 2, zatim idu 3, 5, 7, 11, 13, …)

#include <stdio.h>
#include <math.h>

int main() {

	int n, i, j, c = 0, p = 0,suma = 0;

	scanf_s("%d", &n);

	for (i = 2;p < n; i++ ) {
		c = 0;
		for (j = 1; j <= sqrt(i); j++) {
			if (i % j == 0) {
				c++;
			}
		}
		if (c == 2) {
			printf("%d ", i);
			p++;
			suma = suma + i;
		}
	}
	printf("\n");
	printf("%d\n", suma);


	return 0;
}