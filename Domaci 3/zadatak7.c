//7. Broj je Haršadov ako je deljiv sumom svojih cifara. Dat je broj n. Ispitati da li je broj n Haršadov.

#include <stdio.h>

int main() {
	int n, s = 0, d;

	scanf_s("%d", &n);
	d = n;
	while (d != 0) {
		s = s + (d % 10);
		d = d / 10;
	}

	if (n%s == 0)
		printf("Broj %d je Harsadov broj. \n", n);
	return 0;
}
