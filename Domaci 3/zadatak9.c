//9. Dat je broj n. Izračunati sumu 1^3 + 2^3 + 3^3 + .. + n^3 = 1 + 8 + 27 + 64 + .. + n^3.

#include <stdio.h>

int main() {
	int n, i, s = 0, e = 1;

	scanf_s("%d", &n);
	for (i = 1;i <= n;i++) {
		e = i*i*i;
		s = s + e;
	}

	printf("%d", s);
	return 0;
}
