#include <stdio.h>
#include <stdlib.h>
#include <math.h>


int main()
{
	double t = 1, e, f = 1, k = 1, suma, prov, eps, n=3;

	e = n;
	eps = 1e-9;
	suma = 1 + n;
	while (abs(t) >= eps) {
		e = e * n;
		f = f * (k + 1);
		t = e / f;
		k ++;
		suma = suma + t;
	}
	prov = exp(n);

	printf("%.10lf\t%.10lf\n", suma, prov);

	return 0;
}