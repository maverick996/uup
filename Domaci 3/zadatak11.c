// 11. Dat je broj n. Izracunati sa koliko nula se završava n!.

#include <stdio.h>

int main() {

	long long i, j = -1, n, fakt = 1, m, p = 0;

	scanf_s("%d", &n);

	for (i = 1; i <= n;i++) {
		fakt = fakt * i;
	}
	m = fakt % 10;
	while (m == 0) {
		m = fakt % 10;
		j++;
		fakt =  fakt / 10;
	}
	if (j == -1)
		printf("Broj se ne zavrsava sa nulom\n");
	else
		printf("%d\n", j);

	return 0;
}