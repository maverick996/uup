//4.Trka je dugačka L metara.U trci učestvuje n trkača sa rednim brojevima 1, 2, .., n.Trkač sa brojem i trči brzinom v_i metara u sekundi.
//Ispisati broj trkača koji će najbrže da završi trku kao i njegovo vreme trčanja, zatim broj trkača i njegovo vreme koji drugi završi trku kao i
//broj i vreme trkača koji je stigao treći na cilj.Prvi broj koji se učitaje je L, zatim se učitaje n i onda brojevi v_1 v_2 ..v_n.


#include <stdio.h>

int main() {

	int s, n, i, i1=1, i2=1, i3;
	float t, v, first, second, third;

	scanf_s("%d", &s);
	scanf_s("%d", &n);

	first = second = third = s;

	for (i = 1; i <= n;i++) {
		scanf_s("%f", &v);
		t = s / v;

		if (first > t ) {
			third = second;
			i3 = i2;

			second = first;
			i2 = i1;

			first = t;
			i1 = i;
		}
		else if (second > t) {
			third = second;
			i3 = i2;
			second = t;
			i2 = i;
		}
		else if (third > t) {
			third = t;
			i3 = i;
		}
	}

	printf("%d %.3f\n%d %.3f\n%d %.3f\n", i1,first, i2,second,i3, third);

	return 0;
}