// 8.  Data su dva broja n i m. Izračunati njihov najmanji zajednički sadržalac, tj. NZS(n, m). Uputstvo: NZS(n, m) = n * m / NZD(n, m).

#include <stdio.h>

int main() {
	int n, m, r, sm, sn, nzs;
	scanf_s("%d%d", &n, &m);
	sn = n; 
	sm = m;
	while (n != 0 && m != 0) {
		if (n > m)
			n = n % m;
		else
			m = m % n;
	}
	if (n != 0) {
		nzs = (sn * sm) / n;
	}
	else {
		nzs = (sn * sm) / m;
	}

	printf("NZS(%d, %d) = %d\n", sn, sm, nzs);

	return 0;
}
