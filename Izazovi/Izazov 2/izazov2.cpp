/* Naci broj trojki (a, b, c) prirodnih brojeva tako da je 1 <= a <= b <= c <= n i da vazi a^2 + b^2 = c^2
   Na mail poslati resenje za 9888 */
#include <stdio.h>
#include <math.h>

int main()
{
    int a,b,c,n,br=0;
    scanf("%d",&n);
        for (a=1;a<=n;a++)
        for (b=a;b<=n;b++)
        {
            int c = (int) (sqrt(a*a + b*b) + 1e-9);
            if ((c*c == a*a + b*b) && (c<=n)) { br++; /*printf("%d %d %d\n", a,b,c)*/; }
        }
    printf("%d\n",br);

    return 0;
}
//input: 9888     outpur: 12327
