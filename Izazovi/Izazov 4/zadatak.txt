Dat je broj n. Izracunati na koliko nacina osobe 1, 2, .., n mogu da rasporede n sesira sa oznakama 1, 2, .., n tako da nijedna osoba sa oznakom i ne nosi sesir sa brojem i. Tj. nijedna osoba ne nosi sesir sa istim brojem kao sto je broj te osobe.
ULAZ 			IZLAZ
3 			  2
Jedine mogucnosti su: 231 312. Kombinacija 213 nije moguca jer osoba 3 nosi sesir sa oznakom 3. Isto tako 321 nije moguce jer osoba 2 nosi sesir sa oznakom 2. Kombinacije 123 i 132 nisu moguce jer osoba 1 nosi sesir sa oznakom 1.
Na mail mi poslati resenje za n = 17
