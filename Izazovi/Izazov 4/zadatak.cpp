#include <stdio.h>

unsigned long long fakt(int n) {
	int i;
	unsigned long long rez = 1;
	for (i = 1;i <= n;i++)
		rez = rez*i;
	return rez;
}


int main() {
	
	int i, n,c;
	unsigned long long d;
	//scanf_s("%d", &n);
	for (n = 0;n <= 17;n++) {
		int s = 1;
		long double z = 1;
		for (i = 1;i <= n;i++) {
			s *= -1;
			z += s * 1.0 / fakt(i);
		}
		d = fakt(n) * z;
		printf("%d\t%llu\n",n, d);
	}
	return 0;
}