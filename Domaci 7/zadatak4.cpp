#include <stdio.h>
#include <stdlib.h>
#include <math.h>

double harmSredina(int n, double *a) {
	double suma=0;
	for (int i = 0;i < n;i++)
		suma += 1.0 / a[i];
	return n / suma;
}

double geoSredina(int n, double *a) {
	double proiz = 1;
	for (int i = 0;i < n;i++)
		proiz *= a[i];
	return pow(proiz,1.0/n);
}

int main() {
	double *a;
	int n;
	scanf_s("%d", &n);
	a = (double *)malloc(n*sizeof(double));
	// Ucitavanje niza
	for (int i = 0;i < n;i++)
		scanf_s("%lf", &a[i]);

	printf("Harmonijska sredina: %lf\n", harmSredina(n, a));
	printf("Geometrijska sredina: %lf\n", geoSredina(n, a));
	return 0;
}