#include <stdio.h>
#include <stdlib.h>


double jedi(double s0, int n, double *p){
	double suma = s0;
	for (int i = 0;i < n;i++) {
		if (0.8*suma > p[i]) { 
			suma += p[i];
		}
	}
	return suma;
}

int main() {
	double s0, *p;
	int n;
	scanf_s("%d %lf", &n, &s0);
	p = (double *)malloc(n*sizeof(double));
	// Ucitavanje protivnika
	for (int i = 0;i < n;i++)
		scanf_s("%lf", &p[i]);

	printf("%lf", jedi(s0, n, p));
	return 0;
}