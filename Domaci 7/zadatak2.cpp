#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void zameni3(int *a, int *b, int *c) {
	int tmp = *a;
	*a = *b;
	*b = *c;
	*c = tmp;
}

void mesajNiz(int n, int niz[]) {
	for (int i = 0;i < n;i++) {
		int a = niz[rand() % n ];
		int b = niz[rand() % n ];
		int c = niz[rand() % n ];
		zameni3(&a, &b, &c);
		niz[0] = a;
		niz[1] = b;
		niz[2] = c;
	}
}

int main() {
	srand(time(NULL));
	int n, *a;
	scanf_s("%d", &n);
	a = (int *)malloc(n*sizeof(int));
	// Generisi niz
	for (int i = 0;i < n;i++) 
		a[i] = i + 1;
	// Mesaj niz
	mesajNiz(n, a);
	// Stampaj niz
	for (int i = 0;i < n;i++)
		printf("%d", a[i]);
	printf("\n");
	return 0;
}