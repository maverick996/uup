#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#define doubleNiz 200


void polinom(int n, int *a, double v) {
	double p=0;
	int j = 0;
	for (int i = n-1; i >= 0;i--) {
		p += a[j] * pow(v, i);
		j++;
	}
	printf("Resenje: %lf \n", p);
}

int main() {
	int n, j = 0, *a;
	double v[doubleNiz];
	scanf_s("%d", &n);
	// Dinamicki niz
	a = (int *) malloc(n*sizeof(int));
	
	for (int i = 0; i < n;i++)
		scanf_s("%d", &a[i]);
	do {
		scanf_s("%lf", &v[j]);
		polinom(n, a, v[j]);
		j++;
	} while (v[j-1]);

	free(a);
	return 0;
}