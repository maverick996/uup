/*
2. Na ulazu se ucitava matrica dimenzija 2x2. Na izlazu odstampati vrednost determinante kao decimalan broj sa 8 cifara nakon decimalne tacke.
*/
#include<stdio.h>

void main() {
	int i, j;
	float n[2][2],d;

	for (i = 0;i < 2;i++) // Petlja za red
		for (j = 0;j < 2;j++) { // Petlja za kolonu
			printf_s("Unesi element a[%d][%d]: ", i,j);
			scanf_s("%f", &n[i][j]); // Ucitavamo matricu
		}

	d = (n[0][0] * n[1][1]) - (n[0][1] * n[1][0]); // Odredjujemo determinantu

	printf_s("%.8f \n", d);
		
}