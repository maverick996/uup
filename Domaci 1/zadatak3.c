/*
3. Na ulazu se ucitavaju tri decimalna broja koji predstavljaju duzine stranica trougla (necemo sada proveravati da li je trougao moguc). Na izlazu ispisati povrsinu trougla koji obrazuju te tri stranice u formatu (sve brojeve stampati na 4 decimale):
Povrsina trougla cije su stranice duzina <PRVA_STRANICA>, <DRUGA_STRANICA> i <TRECA_STRANICA> je <POVRSINA_TROUGLA>.
*/

#include<stdio.h>
#include<math.h> // Obavezno zbog korena SQRT

void main() {
	float a, b, c,o,p,s;
	// Ucitavanje stranice
	printf_s("Stranica a: ");
	scanf_s("%f", &a);

	printf_s("Stranica b: ");
	scanf_s("%f", &b);

	printf_s("Stranica c: ");
	scanf_s("%f", &c);
	// Kraj ucitavanja stranice
	o = a + b + c; // Izracunavanje obima
	s = o / 2; // Izracunavanje S
	p = sqrt(s*(s - a)*(s - b)*(s - c)); // Racunanje P

	printf_s("Povrsina trougla cije su stranice duzina %.4f %.4f  i %.4f je %.4f \n", a, b, c, p);
}