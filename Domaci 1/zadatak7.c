/*
7. Na ulazu se ucitavaju dva realna broja a i b. Na izlazu odstampati a^b (a stepenovano na b) na 5 decimala.
*/

#include<stdio.h>
#include<math.h> // Obavezno zbog pow

void main() {
	float a, b;
	// Ucitavanje podataka
	printf_s("Unesi broj a: ");
	scanf_s("%f", &a);

	printf_s("Unesi broj b: ");
	scanf_s("%f", &b);
	// Zavrseno ucitavanje
	printf_s("Proizvod: %.5f\n", pow(a,b));
}