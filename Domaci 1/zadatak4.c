/*
4. Data je duz tako sto su date x i y koordinate oba kraja te duzi (ulaz je dat kao x1 y1 x2 y2 gde je (x1, y1) tacka jednog a (x2, y2) 
tacka drugog kraja duzi). Na izlazu odstampati duzinu te duzi na 6 decimala.
*/


#include<stdio.h>
#include<math.h> // Obavezno zbog funkcija SQRT i POW

void main() {
	int x1, x2, y1, y2;
	float d;
	// Ucitavanje
	printf_s("Kordinata x1: ");
	scanf_s("%d", &x1);

	printf_s("Kordinata y1: ");
	scanf_s("%d", &y1);

	printf_s("Kordinata x2: ");
	scanf_s("%d", &x2);

	printf_s("Kordinata y2: ");
	scanf_s("%d", &y2);
	// Kraj ucitavanja
	d = sqrt(pow((x1 - x2),2)  + pow((y1 - y2),2)); // Racunanje duzina, funkcija pow je za stepen - pow(baza, eksponent)

	printf_s("Duzina duzi: %.6f\n", d);

}