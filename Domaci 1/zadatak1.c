/*
1. Na ulazu se ucitava petocifren broj. Na izlazu odstampati sumu cifara broja.
*/

#include<stdio.h>

void main() {
	int n,sum=0,r;
	printf_s("Unesi broj\n");
	scanf_s("%d", &n); // Ucitavanje broja N

	while (n != 0) { // Deli broj sve dok ne dodje do 0 
		r = n % 10; // Odvajavno cifre (cifru po cifru) / % = mod u pascal-u
		sum = sum + r; // Sabiramo odvojene cifre
		n = n / 10; // "oduzimamo" cifru koju smo sabrali
	}
	printf_s("Suma brojeva: %d \n", sum); // Izbacujemo rezultat

}