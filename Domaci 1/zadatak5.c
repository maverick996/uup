/*
5. Torta se seca na n delova (n se ucitava sa ulaza). Odstampati maksimalan broj parcica torte koje mozemo da dobijemo kada optimalno secemo 
(optimalno secenje je kada dobijemo maksimalan broj parcica). Npr: za 1 secenje maksimalan broj je 2 parceta, za 2 secenja 4 parceta, za 3 secenja je resenje 7 parceta.

*/

#include<stdio.h>

void main() {
	int n;
	float p;

	printf_s("Broj secenja: ");
	scanf_s("%d", &n);

	p = (n*n + n + 2) / 2;

	printf_s("Broj parceta: %.0f\n", p);
}