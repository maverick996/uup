#include <stdio.h>
#define MAXR 100
#define MAXC 100

int fibonaci(int n) {
	int s, p = 0, t = 1;
	for (int i = 0; i < n;i++) {
		s = p + t;
		p = t;
		t = s;
	}
	return p;
}

void fiboMat(int n, int m, int mat[MAXR][MAXC]) {
	for (int i = 0; i < n; i++)
		for (int j = 0; j < m; j++)
			mat[i][j] = fibonaci(i + j);
}


int main(){
	int n,m;
	int a[MAXR][MAXC];
	scanf_s("%d %d", &n,&m);
	fiboMat(n, m, a);
	for (int i = 0; i < n; i++) {
		printf("| ");
		for (int j = 0; j < m; j++)
			printf("%d ", a[i][j]);
		printf("|\n");
	}
		
	return 0;
}
