#include <stdio.h>
#define MAXR 100
#define MAXC 100

long long fakt(int n) {
	int rez = 1;
	for (int i = 1;i <= n;i++)
		rez *= i;
	return rez;
}

void copy(int n, int m, int src[MAXR][MAXC], int dest[MAXR][MAXC]) {
	for (int i = 0; i < n; i++)
		for (int j = 0; j < m; j++)
			dest[i][j] = src[i][j];
}

void stampaj(int n, int m, int mat[MAXR][MAXC]) {
	for (int i = 0; i < n; i++) {
		printf("| ");
		for (int j = 0; j < m; j++)
			printf("%d ", mat[i][j]);
		printf("|\n");
	}
}

void sumMat(int n, int m, int mat[MAXR][MAXC]) {
	for (int i = 0; i < n; i++)
		for (int j = 0; j < m; j++) {
			if ((i == 0 || j == 0))
				mat[i][j] = 1;
			else
				mat[i][j] = mat[i - 1][j] + mat[i][j - 1] + mat[i - 1][j - 1];
		}
	stampaj(n, m, mat);
}

void katalanMat(int n, int m, int mat[MAXR][MAXC]) {
	for (int i = 0; i < n; i++)
		for (int j = 0; j < m; j++) {
			int m = i + j;
			int n = 2 * m;
			mat[i][j] = (1.0 / (m + 1))*(fakt(n) / (fakt(m)*fakt(n-m)));
		}
	stampaj(n, m, mat);

}

void mnozi(int n, int m, int mat1[MAXR][MAXC], int mat2[MAXR][MAXC],int matRez[MAXR][MAXC]) {
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < m; j++) {
			matRez[i][j] = 0;
			for (int k = 0; k < m; k++)
				matRez[i][j] = matRez[i][j] + mat1[i][k] * mat2[k][j];
		}
	}
	stampaj(n, m, matRez);
}


int main(){
	int n,m;
	int a[MAXR][MAXC], sum[MAXR][MAXC], rez[MAXR][MAXC];
	scanf_s("%d %d", &n,&m);
	printf("Suma: \n");
	sumMat(n, m, a);

	copy(n, m, a, sum); // Moram na neki nacin da sacuvam prethodnu matricu
	printf("\n Katalan: \n");
	katalanMat(n, m, a);

	printf("\n Proizvod: \n");
	mnozi(n, m, sum, a, rez);
		
		
	return 0;
}
