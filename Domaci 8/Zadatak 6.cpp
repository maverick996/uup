#include <stdio.h>
#define MAXR 100
#define MAXC 100

typedef bool (*funk)(double,double);

void zameni(double *a, double *b) {
	double tmp = *a;
	*a = *b;
	*b = tmp;
}

bool desc(double a, double b) {
	if (a < b) return true;
	return false;
}

bool asce(double a, double b) {
	if (a > b) return true;
	return false;
}

void stampaj(int n, int m, double mat[MAXR][MAXC]) {
	for (int i = 0; i < n; i++) {
		printf("| ");
		for (int j = 0; j < m; j++)
			printf("%.1lf ", mat[i][j]);
		printf("|\n");
	}
}

void sortMat(int n, int m, double mat[MAXR][MAXC], funk vrsta) {
	int z = 0, q = 0,w;
	double min,tmp;
	for (int i = 0;i < n;i++)
		for (int j = 0;j < m;j++) {
			z = i; q = w = j;
			min = mat[i][j];
			for (int k = i;k < n;k++) {
				for (;w <= n; w++)
					if (vrsta(mat[k][w],min)){
						min = mat[k][w];
						z = k;
						q = w;
					}
				w = 0;
			}
			zameni(&mat[i][j], &mat[z][q]);
		}
}

int main() {
	int n, m;
	double a[MAXR][MAXC];
	scanf_s("%d %d", &n, &m);
	for (int i = 0; i < n; i++)
		for (int j = 0; j < m; j++)
			scanf_s("%lf", &a[i][j]);
	
	sortMat(n, m, a, asce);
	stampaj(n, m, a);
	printf("\n");
	sortMat(n, m, a, desc);
	stampaj(n, m, a);

}