#include <stdio.h>
#define MAXC 100

double sumaMat(int n, int m, double mat[][MAXC]) {
	double suma = 0;
	for (int i = 0; i < n; i++)
		for (int j = 0;j < m; j++)
			suma += mat[i][j];
	return suma;

}
double srednjaVrednostMat(int n, int m, double mat[][MAXC]) {
	return sumaMat(n, m, mat) / (n * m);
}


int main(){
	int n,m;
	double a[100][MAXC];
	scanf_s("%d %d", &n,&m);
	for (int i = 0; i < n; i++) 
		for (int j = 0; j < m; j++) 
			scanf_s("%lf", &a[i][j]);
	printf("%.3lf \n", sumaMat(n, m, a));
	printf("%.3lf \n", srednjaVrednostMat(n, m, a));
	return 0;
}
