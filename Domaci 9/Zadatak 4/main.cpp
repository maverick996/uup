#include <stdio.h>
#include <string.h>

// Format: 30.12.2015-21.1.2016

#define prestupna(godina) (!((godina)%((godina) % 25 ? 4:16)))
#define dani(mesec, godina) (".dadcdcddcdcd"[mesec]-69+(prestupna(godina)&&((mesec)==2))) // Racunanje broj dana preko stringa (ascii vrednosti)

typedef struct datum {
	int dan;
	int mesec;
	int godina;
} datum;

// Funkcija da prebaci string u int, preko ascii-a
int strtoint(char* s) {
	int d = strlen(s), br = 0; // br mora da bu 0 zato sto u prvom koraku je cifra uvek na mestu jedinice.
	for (int i = 0; i < d;i++)
		br = (s[i] - '0') + br * 10; // Bilo koji broj - char '0' je broj (npr '8' = 56, '0'= 48, '8'-'0' = 56 - 48 = 8), * 10 je ako ima vise od jedno broja da bi se dodavalo
	return br;
}

void datumRazdvoji(char *s, datum &pocetniD, datum &krajnjiD) {
	char* pocetni;
	char* krajnji;

	// Razdvojiti pocetni i krajnji datum, do -
	pocetni = strtok(s, "-");
	krajnji = strtok(NULL, "");
	// Upisati datume u strukturu

	pocetniD.dan = strtoint(strtok(pocetni, "."));
	pocetniD.mesec = strtoint(strtok(NULL, "."));
	pocetniD.godina = strtoint(strtok(NULL, ""));

	krajnjiD.dan = strtoint(strtok(krajnji, "."));
	krajnjiD.mesec = strtoint(strtok(NULL, "."));
	krajnjiD.godina = strtoint(strtok(NULL, ""));
}

int datumRacunaj(char* s) {
	int ostatakMesecaD, ostaliMeseci = 0, godine = 0;
	datum pocetniD, krajnjiD;
	
	datumRazdvoji(s, pocetniD, krajnjiD);

	// Debug printf("%d/%d/%d - %d/%d/%d\n", pocetniD.dan, pocetniD.mesec, pocetniD.godina,krajnjiD.dan,krajnjiD.mesec,krajnjiD.godina);

	if (krajnjiD.godina < pocetniD.godina) return -1; // Nema razloga da radi, datum je pogresno unet

	if (pocetniD.godina == krajnjiD.godina) {
		if (pocetniD.mesec == krajnjiD.mesec)
			return krajnjiD.dan - pocetniD.dan;
		else {
			ostatakMesecaD = dani(pocetniD.mesec, pocetniD.godina) - pocetniD.dan; // Ostatak do kraja meseca
			for (int i = pocetniD.mesec + 1; i < krajnjiD.mesec;i++) // Ovo racuna sve mesece izmedju ali od prvog sledeceg do pretposlednjeg
				ostaliMeseci += dani(i, pocetniD.godina);
			return ostatakMesecaD + ostaliMeseci + krajnjiD.dan; // Dodamo razliku do kraja meseca, zatim broj dana da dodjemo do krajnjeg meseca i na kraju dodamo dane u krajnjem mesecu
		}
	}
	else {
		if (pocetniD.mesec == krajnjiD.mesec) {
			int krajGodineM = -pocetniD.dan;

			for (int i = pocetniD.mesec; i <= 12; i++) {
				krajGodineM += dani(i,pocetniD.godina);
			}

			for (int i = pocetniD.godina + 1;i < krajnjiD.godina; i++) { // Sabira godine izmedju
				if(prestupna(i))
					godine += 366;
				else godine += 365;
			}
			int pocetakGodineM = krajnjiD.dan;
			for (int i = 1; i < krajnjiD.mesec;i++)
				pocetakGodineM += dani(i, krajnjiD.godina);
			return godine + krajGodineM + pocetakGodineM;
		}
		int ostatakDana = dani(pocetniD.mesec,pocetniD.godina) - pocetniD.dan; // Broj dana do kraja trenutnog meseca

		 // Broj dana od sledeceg meseca do kraja godine
		int ostatakMesecaP = 0;
		for (int i = pocetniD.mesec + 1; i <= 12; i++)
			ostatakMesecaP += dani(i, pocetniD.godina); 

		// Broj dana od pocetka krajnjeg datuma pa do krajnjeg meseca
		int ostatakMesecaK = 0;
		for (int i = 1; i < krajnjiD.mesec; i++)
			ostatakMesecaK += dani(i, krajnjiD.godina);

		// Broj dana od pocetne godine do krajnje godine
		int pocetakGodine = 0;
		for (int i = pocetniD.godina + 1;i < krajnjiD.godina; i++) { // Sabira godine izmedju
			if (prestupna(i))
				pocetakGodine += 366;
			else pocetakGodine += 365;
		}

		return ostatakDana + ostatakMesecaP + ostatakMesecaK + krajnjiD.dan + pocetakGodine;
	}
	
}

int main() {
	char string[100];
	int k = 0,n;

	FILE* in = fopen("ulaz4.txt", "r");
	FILE* out = fopen("izlaz4.txt", "w");

	if (in == NULL) printf("Fajl nije ucitan \n");
	else {
		while (!feof(in)) {
			fscanf(in, "%s", string);
			fprintf(out,"%d\n",datumRacunaj(string));
		}
		fcloseall; 
	}
}