#include <stdio.h>
#include <string.h>

int main(){
    char c;
    int chars[127] = {0}; // Array svih karaktera
    int m=0, v= 0, r=0, l=1;
    FILE* in = fopen("ulaz.txt","r" );
    FILE* out = fopen("izlaz.txt","w");

    if(in == NULL) printf("File nije pronadjen");
    else{
        while(!feof(in)){
            c = fgetc(in);
            if(c != EOF){
               if(c == '\n') l++;
               if(c == ' ') r++;
               if((int)c >= 65 && (int)c <= 90) v++;
               if((int)c >= 97 && (int)c <= 122) m++;
               chars[(int)c]++; // Svaki karakter se u array povecava od nule
            }

        }
        fclose(in);
    }

    fprintf(out,"Razmak: %d\n",r);
    fprintf(out,"Redova: %d\n",l);
    fprintf(out,"Reci: %d\n", r + l);
    fprintf(out,"Velika slova: %d \n",v);
    fprintf(out,"Mala slova: %d \n",m);
    fprintf(out,"\nPojavljivanje slova: \n\n");
    for (int i = 65; i <= 122; i++) {
        if(chars[i] > 0){
            if(i==97) printf("\n");
            fprintf(out,"%c - %d \n", i, chars[i]);
        }
    }
    if(out != NULL) printf("Fajl uspesno upisan");

    fclose(out);

    return 0;
}

