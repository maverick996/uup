#include <stdio.h>
#include <string.h>

#define ALPHA 62 // Broj slova,brojeva u nizu

int trazi(const char c, const char *dest){
	for (int i = 0; i < ALPHA; i++)
		if (c == dest[i])
			return i;
	return -1;
}

void generisi(char n[ALPHA]) {
	int i, j, k = 0;
	// a..z
	for (i = 97; i <= 122; i++) {
		n[k] = (char)i;
		k++;
	}
	// A..Z
	for (i = 65; i <= 90; i++) {
		n[k] = (char)i;
		k++;
	}
	// 0..9
	for (i = 48; i <= 57; i++) {
		n[k] = (char)i;
		k++;
	}
}

char sifruj(const char c, const char n[ALPHA], const int k) {
	int rez = trazi(c, n);
	if (rez != -1) {
		if (rez + k >= ALPHA) return n[(rez + k) % ALPHA];
		return n[rez + k];
	}
	return c;
}

int main() {
	char c;
	char alpha[ALPHA];
	int n;

	FILE* in = fopen("ulaz6.txt", "r");
	FILE* out = fopen("izlaz6.txt", "w");

	generisi(alpha);
	scanf("%d", &n);
	if (in == NULL) printf("Fajl nije ucitan \n");
	else {
		while (!feof(in)) {
			c = fgetc(in);
			fprintf(out,"%c", sifruj(c, alpha, n));

		}
		fcloseall; 
	}
}