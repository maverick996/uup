#include <stdio.h>
#include <string.h>


int uporedi(const char* a, const char* b) {
	int k = 0;
	while (a[k] == b[k]) {
		if (a[k] == '\0' || b[k] == '\0') // Doslo je do kraja (jednog) stringa
			break; // Izlazi iz while-a
		k++;
	}
	if (a[k] == '\0' && b[k] == '\0') // Ovo proverava da li su oba stringa do kraja, da ne bi doslo do greske kao TEST == TEST(IRANJE)
		return 1;
	else
		return 0;

}

int main() {
	char input[100];
	char string[100];
	int k = 0;

	FILE* in = fopen("ulaz2.txt", "r");
	FILE* out = fopen("izlaz2.txt", "w");

	scanf("%s", input);

	if (in == NULL) printf("Fajl nije ucitan \n");
	else {
		while (!feof(in)) {
			fscanf(in, "%s", string);
			if (uporedi(input, string)) k++;
			
		}
		fprintf(out, "Rec '%s' se ponavlja %d puta", input, k);
		fcloseall; // Zatvori sve fajlove od jednom moze i fclose(in); fclose(out);
	}
}