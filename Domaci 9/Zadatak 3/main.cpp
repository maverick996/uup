#include <stdio.h>
#include <string.h>

char malo(char a) {
	a = (int)a; // Prebaci u ascii
	if (a >= 65 && a <= 90) return (char)a + 32; // Razlika izmedju malog i velikog slova 97 - 65
	return (char)a;
};

int duzina(char* a) {
	int k = 0;
	while (a[k] != '\0') k++;
	return k;
};

int uporedi(char* in, char* word, int v, int* m, char* rec) {
	int k = 0;
	int i = 0;
	int p = 0;
	int t = 0;

	while (in[k] != '\0') {
		if (malo(in[k]) == malo(word[i])) {
			i++; p++;
		} else { i = 0; p = 0; }
		k++;
		if (p == duzina(word)) {
			i = 0; 
			p = 0;
			t++;
			if (v == 0) {
				*m = k - 2;
				strcpy(rec, in);
			}
			// Debug printf("%s\n", in);
			
		};
		
	}
	
	return t; // Vraca T zato sto moze u jednoj reci vise puta da bude rec
};


int main() {
	char input[100];
	char string[100];
	char rec[100];
	int k = 0,m;
	int v = 0;

	FILE* in = fopen("ulaz3.txt", "r");
	FILE* out = fopen("izlaz3.txt", "w");

	scanf("%s", input);

	if (in == NULL) printf("Fajl nije ucitan \n");
	else {
		while (!feof(in)) {
			fscanf(in, "%s", string);
			if(uporedi(string, input, v, &m, rec))
			k += uporedi(string, input, v++, &m, rec);

		}
		fprintf(out,"Rec '%s' se ponavlja %d puta \n",input, k);
		fprintf(out, "Prva rec u kojoj je pronadjena rec '%s' je: '%s'\n", input, rec);
		fprintf(out, "Na %d. mestu \n", m);
        
		fcloseall; 
	}
}