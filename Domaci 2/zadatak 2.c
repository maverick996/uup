/* Unose se četiri cela broja. Na izlazu ispisati maksimalan i minimalan od ta četiri broja.*/

#include <stdio.h>

int main(int argc, const char * argv[]) {
	int b1, b2, b3, b4, min, max;

	printf("Broj 1 \n");
	scanf("%d", &b1);

	printf("Broj 2 \n");
	scanf("%d", &b2);

	printf("Broj 3 \n");
	scanf("%d", &b3);

	printf("Broj 4 \n");
	scanf("%d", &b4);

	max = b1;
	min = b1;

	if (max < b2)
		max = b2;
	if (max < b3)
		max = b3;
	if (max < b4)
		max = b4;

	if (min > b2)
		min = b2;
	if (min > b3)
		min = b3;
	if (min > b4)
		min = b4;

	printf("Najmanji broj je %d a najveci %d \n", min, max);

	return 0;
}
