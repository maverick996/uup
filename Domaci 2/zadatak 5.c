/* Na ulazu je dat broj poena studenta na pismenom ispitu. Na izlazu ispisati ocenu koju je student dobio 
(0-50: pet, 51-60: sest, 61-70: sedam, 71-80: osam, 81-90: devet, 91-100: deset).
*/

#include <stdio.h>

int main(int argc, const char * argv[]) {
	int p;
	char o;

	printf("Unesi broj poena\n");
	scanf("%d", &p);
	
	if (p >= 0 && p <= 50)
		printf("Vasa ocena je pet \n");
	else if (p >= 51 && p <= 60)
		printf("Vasa ocena je sest \n");
	else if (p >= 61 && p <= 70)
		printf("Vasa ocena je sedam \n");
	else if (p >= 71 && p <= 80)
		printf("Vasa ocena je osam \n");
	else if (p >= 81 && p <= 90)
		printf("Vasa ocena je devet \n");
	else if (p >= 91 && p <= 100)
		printf("Vasa ocena je deset ! \n");
	else
		printf("Nemoguc broj poena. \n");

	return 0;
}