/* Učitaje se broj koji predstavlja dan D u oktobru 2015 godine. Ispisati naziv dana koji ima datum D. oktobar 2015.*/

#include <stdio.h>

int main(int argc, const char * argv[]) {
    int d;

    printf("Unesi dan\n");
    scanf("%d",&d);
    if(d<=31 && d>=1){
    switch (d % 7) {
        case 1:
            printf("Cetvrtak");
            break;
        case 2:
            printf("Petak");
            break;
        case 3:
            printf("Subota");
            break;
        case 4:
            printf("Nedelja");
            break;
        case 5:
            printf("Ponedeljak");
            break;
        case 6:
            printf("Utorak");
            break;
        case 7:
        case 0:
            printf("Sreda");
            break;
            
        default:
            break;
    }
    } else{
        printf("Oktrobar ima 31 dan, molim te unesi broj u opsegu 1-31");
    }
    
    printf("\n");
    
    return 0;
}
