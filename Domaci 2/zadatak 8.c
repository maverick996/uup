/* Dat je broj n. Ispisati f_n: n-ti član Fibonačijevog niza. (f_0 = 0, f_1 = 1, f_n = f_{n-1} + f_{n-2}, tj. svaki naredni član jednak je zbiru prethodna dva,
 f_2 = 1, f_3 = 2, f_4 = 3, …) Pročitati napomenu: nema korišćenja nizova i ciklusa.*/

#include <stdio.h>
#include <math.h>

int main(int argc, const char * argv[]) {

	int n;
	long f;

	printf("Unesi n \n");
	scanf("%d", &n);

	f = round(0.44721359549995682 * pow(1.6180339887498949, n));

	printf("%d\n", f);
	return 0;
}