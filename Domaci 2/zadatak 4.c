/* Na ulazu se unosi jedno slovo (slova koja su moguća na ulazu su abcdefghijklmnoprstuvzABCDEFGHIJKLMNOPRSTUVZ). 
Na izlazu ispisati da li je unešeno slovo samoglasnik ili suglasnik.*/

// Prvi nacin, koriscene su funkcije

#include <stdio.h>
#include <ctype.h>

int main(int argc, const char * argv[]) {
	char c;
	int e;

	printf("Unesi slovo\n");
	scanf("%c", &c);

	if (isalpha(c)) {
		switch (tolower(c)) {
			case 'a':
			case 'e':
			case 'i':
			case 'o':
			case 'u':
				e = 1;
				break;
			default:
				e = 0;
				break;
		}
	}
	else {
		printf("To nije slovo\n");
	}

	if (e == 1)
		printf("Slovo %c je samoglasnik.\n", c);
	else
		printf("Slovo %c je suglasnik.\n", c);
	return 0;
}

// Drugi nacin, bez funkcija, pomocu ASCII table.

#include <stdio.h>

int main(int argc, const char * argv[]) {
	char c,e;

	printf("Unesi slovo\n");
	scanf("%c", &c);
	e = c; // Cuvamo prvobitno slovo, zato sto se kasnije prebacuje u veliko.
	if ((c >= 65 && c <= 90) || (c >=97 && c<=121)) {
		if (c > 90)
			c = c - 32;
		switch (c) {
			case 'A':
			case 'E':
			case 'I':
			case 'O':
			case 'U':
				printf("Slovo %c je samoglasnik.\n", e);
				break;
			default:
				printf("Slovo %c je suglasnik.\n", e);
				break;
		}
		
	}
	else
		printf("To nije slovo\n");

	return 0;
}

