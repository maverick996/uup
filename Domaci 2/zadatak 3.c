/* Date su dve prave p1 i p2. Prava p1 prolazi kroz RAZLIČITE tačke (x1, y1) i (x2, y2), dok prava p2 prolazi kroz RAZLIČITE tačke (x3, y3) i (x4, y4). 
Na ulazu se upisuju redom x1 y1 x2 y2 x3 y3 x4 y4. Na izlazu ispisati da li su prave paralelne ili se seku.*/

#include <stdio.h>

int main(int argc, const char * argv[]) {

	int x1, x2, x3, x4, y1, y2, y3, y4, a1, a2, b1, b2, c1, c2, d;
	float x ,y;
	
	printf("Unesi pravu u sledecem formatu (x1,y1) i (x2,y2)\n");
	scanf("%d%d%d%d", &x1, &y1, &x2, &y2);

	printf("Unesi pravu u sledecem formatu (x3,y3) i (x4,y4)\n");
	scanf("%d%d%d%d", &x3, &y3, &x4, &y4);
	
	a1 = y2 - y1;
	b1 = x1 - x2;
	c1 = a1*x1 + b1*y1;

	a2 = y4 - y3;
	b2 = x3 - x4;
	c2 = a2*x3 + b2*y3;

	d = a1*b2 - a2*b1;

	if (d == 0) {
		printf("Prave su paralelne \n");
	}
	else {
		x = (b2*c1 - b1*c2) / d;
		y = (a1*c2 - a2*c1) / d;
		printf("%.0f %.0f\n", x,y);
	}
	return 0;
}
