/*
3. Napisati funkcije:
    double dis2(double x1, double y1, double x2, double y2) koja vraća rastojanje između tačaka (x1,y1) i (x2,y2) - tj. rezultat sqrt((x1-x2)^2+(y1-y2)^2)
    double area(double x1, double y1, double x2, double y2, double x3, double y3) koja vraća površinu trougla sa tačkama (x1,y1), (x2,y2), (x3,y3)
    Za računanje površine koristiti formulu sqrt(s*(s-a)*(s-b)*(s-c)) gde je s = (a+b+c)/2 dok su a, b i c dužine stranica trougla. Koristiti dis2 da bi izračunali dužine stranica trougla. Na ulazu se daju koordinate tri temena trougla x1, y1, x2, y2, x3, y3 na ekranu ispisati rezultat koji vrati funkcija area(x1, y1, x2, y2, x3, y3).
    ULAZ                    IZLAZ
    0 0 1 0 0 1             0.5000000
*/

#include <stdio.h>
#include <math.h>

double dis2(double x1, double y1, double x2, double y2) {
	return sqrt(pow((x1 - x2), 2) + pow((y1 - y2), 2));
}

double area(double x1, double y1, double x2, double y2, double x3, double y3) {
	double s, a, b, c;
	a = dis2(x1, y1, x2, y2);
	b = dis2(x1, y1, x3, y3);
	c = dis2(x2, y2, x3, y3);
	s = (a + b + c) / 2;
	return sqrt(s*(s - a)*(s - b)*(s - c));
}

int main() {
	double x1, x2, x3, y1, y2, y3;
	scanf_s("%lf%lf%lf%lf%lf%lf", &x1,&y1,&x2,&y2,&x3,&y3);
	
	printf("%lf\n", area(x1,y1,x2,y2,x3,y3));
}