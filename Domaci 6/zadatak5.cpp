/*
5. Napisati funkcije:
    int sumaCifara(int n) - koja vraća sumu cifara broja n
    int jeNiven(int n) - koja vraća true ako je broj n deljiv sumom svojih cifara
    int naredniNiven(int n) - koja vraća naredni Nivenov broj nakon n, tj. prvi od n + 1, n + 2, n + 3... za koji važi da funkcija jeNiven vraća true, tj. prvi od n + 1, n + 2, n + 3, ... koji je deljiv sumom svojih cifara
    double sum(double x, double eps) - funkcija koja računa sledeću sumu:
    sin(x^N1)/N1 - sin(x^N2)/N2 + sin(x^N3)/N3 - ... 
    Gde su N1, N2, N3, ... redom Nivenovi brojevi (npr. N1 = 1, N2 = 2, N3 = 3, ...). Sumu računati dok član ne postane manji od eps po apsolutnoj vrednosti.
    Sa ekrana se unosi x i eps. Izračunati vrednost koju vrati funkcija sum(x, eps).
*/

#include <stdio.h>
#include <math.h>
#include <stdlib.h>

int sumaCifara(int n){
	int i, suma = 0;
	while (n != 0) {
		suma += n % 10;
		n /= 10;
	}
	return suma;
}

int jeNiven(int n) {
	if (n % sumaCifara(n) == 0)
		return 1;
	return 0;
}

int naredniNiven(int n) {
	do {
		n = n + 1;
	} while (jeNiven(n) != 1);
		
	return n;
}

double sum(double x, double eps) {
	double suma = 0, s = 1, p = x;
	int n=1;
	while (fabs(p) >= eps) {
		s *= -1;
		p = sin(pow(x, n)) / n;
		n = naredniNiven(n);
		suma -= s * p;
		
	}
	return suma;
}

int main() {
	double n,eps;
	scanf_s("%lf%lf", &n,&eps);
	printf("%lf", sum(n,eps));
}