/*
7. Napisati funkcije:
    char cezar(char ch, int k) koja na ulazu ima karakter ch koji je veliko slovo ABC...Z i neki broj k
    kao rezultat funkcija vraća slovo ch cirkularno rotirano za k mesta. Naprimer ako je k=0 nema nikakve rotacije pa je cezar('A',0)='A', cezar('B',0)='B', .., cezar('Z',0)='Z'. Ako je k=1 onda je rezultat jedno slovo unapred, tj. cezar('A',1)='B', cezar('B',1)='C', .., cezar('Y',1)='Z', cezar('Z',1)='A'. Ako je k=2 pomereno je za 2 mesta, tj. cezar('A',2)='C', cezar('B',2)='D', .., cezar('Y',2)='A', cezar('Z',2)='B'.
    Sa ekrana se unosi broj k. Zatim se unose redom slova. Prekida se unos kada se unese tačka ('.'). Za svako uneseno slovo ch ispisati na ekranu cezar(ch, k).
    ULAZ                IZLAZ
    2 ABCXYZ.           CDEZAB

*/

#include <stdio.h>

char cezar(char ch, int k) {
	char cezar;
	if (ch >= 65 && ch <= 90) {
		cezar = ch + k;
		if (cezar >= 65 && cezar <= 90)
			return cezar;
		else
			return cezar - 26;
	}
}


int main() {
	char n;
	int k;
	scanf_s("%d",&k);
	do {
		scanf_s("%c", &n);
		printf("%c", cezar(n, k));
	} while (n != 46);
}