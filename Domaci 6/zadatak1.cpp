/*
1. Napisati funkcije:
   bool jeVelikoSlovo(char ch) - koja vraća true ako je ch veliko slovo (ABC...Z), false u suprotnom
   bool jeMaloSlovo(char ch) - koja vraća true ako je ch malo slovo (abc...z), false u suprotnom
   bool jeZnakInterpunkcije(char ch) - koja vraća true ako je ch jedan od znakova .!? false u suprotnom
   bool jeCifra(char ch) - koja vraća true ako je ch cifra (01...9), false u suprotnom
   bool jeZnakPrekida(char ch) - koja vraća true ako je ch neki od znakova #$%&\/*+-{}|~ false u suprotnom
   Sa ekrana se učitaju znakovi. Učitavanje se prekida kada korisnik upiše neki znak ch koji je znak prekida (funkcija jeZnakPrekida(ch) vraća true). Nakon toga na ekranu ispisati broj velikih slova, malih slova, znakova interpunkcije, i cifara koje je korisnik uneo do unosa znaka prekida.
   ULAZ                         IZLAZ
   Da. OVO je Uneo 123          Broj velikih slova: 13
   KORISNIK.#                   Broj malih slova: 6
                                Broj znakova interpunkcije: 2
                                Broj cifara: 3

*/
                                
#include <stdio.h>
// #‎include‬ <stdbool.h> .c  ili da se fukncije pisu sa int (0 i 1)
bool jeVelikoSlovo(char ch) {
	if (ch >= 65 && ch <= 90)
		return true;
	return false;
}

bool jeMaloSlovo(char ch) {
	if (ch >= 97 && ch <= 122)
		return true;
	return false;
}

bool jeCifra(char ch) {
	if (ch >= 48 && ch <= 57)
		return true;
	return false;
}

bool jeZnakInterpunkcije(char ch) {
	if (ch == 33 || ch == 63 || ch == 46)
		return true;
	return false;
}

bool jeZnakPrekida(char ch) {
	switch (ch)
	{
	case 35:
	case 36:
	case 37:
	case 38:
	case 42:
	case 43:
	case 45:
	case 47:
	case 92:
	case 126:
	case 123:
	case 124:
	case 125:
		return true; break;
	default:
		break;
	}
	return false;
}


int main() {
	char n;
	int up, low, num, sign;
	up = low = num = sign = 0;
	// Moze i case
	do {
		scanf_s("%c", &n);
		if (jeMaloSlovo(n))
			low++;
		else if (jeVelikoSlovo(n))
			up++;
		else if (jeZnakInterpunkcije(n))
			sign++;
		else if (jeCifra(n))
			num++;

	} while (jeZnakPrekida(n) == false);

	printf("Broj velikih slova: %d\n", up);
	printf("Broj malih slova: %d\n", low);
	printf("Broj znakova interpunkcije: %d\n", sign);
	printf("Broj cifara: %d \n", num);

	return 0;
}