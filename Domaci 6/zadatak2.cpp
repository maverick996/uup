/*
2. Napisati funkcije:
int nzd(int a, int b) - koja vraća najveći zajednički delilac brojeva a i b
int nzs(int a, int b) - najmanji zajednički sadržalac brojeva a i b (izračunati pomoću nzs = a * b / nzd(a, b))
Sa ekrana se unosi broj n a nakon toga n brojeva a1, a2, .., an. Izračunati najveći zajednički delilac i najmanji zajednički sadržalac brojeva a1, a2, .., an. Koristiti izraze nzd(a1, a2, ..., a_{k-1}, a_k) = nzd(nzd(a1, a2, ..., a_{k-1}), a_k) i nzs(a1, a2, ..., a_{k-1}, a_k) = nzs(nzs(a1, a2, ..., a_{k-1}), a_k) da bi izračunali rezultat.
ULAZ                    IZLAZ
5                       NZD je 6.
36 12 6 18 48           NZS je 144.
*/

#include <stdio.h>

int nzd(int a, int b) {
	int t;
	while (b != 0) {
		t = b;
		b = a % b;
		a = t;
	}
	return a;
}

int nzs(int a, int b) {
	return (a*b) / nzd(a, b);
}

int main() {
	int a, b = 1, n, i, lcm = 1, gcf = 1;
	scanf_s("%d", &n);
	scanf_s("%d", &a);
	gcf = b = a;
	for (i = 1; i < n; i++) {
		scanf_s("%d", &a);
		lcm = nzs(lcm, nzs(a, b));
		gcf = nzd(gcf, nzd(a, b));
		b = a;
	}
	printf("Najmanji zajednicki sadrzalac NZS: %d\n", lcm);
	printf("Najveci zajednicki delilac NZD: %d\n", gcf);

	return 0;
}