#include <stdio.h>
#include <math.h>
#include <stdlib.h>

bool jeProst(int n) {
	int i;
	for (i = 2; i*i <= n;i++)
		if (n % i == 0)
			return false;
	return true;
}

int stepen(int n, int deg, int p) {
	int i, s = 1;;
	for (i = 1;i <= deg;i++)
		s = (s*n) % p;
	return s;
}

bool jePseudoprost(int n) {
	double t,d;
	int i, q, j, s = 0, x = n - 1;
	bool jePseudoslozen;
	if (n == 1)
		return false;
	else if (n == 2 || n == 3)
		return true;
	
	while (x % 2 == 0) {
		x /= 2;
		s++;
	}
	d = (n - 1)/pow(2.0,s);

	for (i = 1;i <= 20;i++) {
		t = rand() % (n-3)+2;
		q = stepen(t,d,n);

		if (q != 1 && q != (n - 1)) {
			jePseudoslozen = true;
			for (j = 1;j < s;j++) {
				q = stepen(q,2,n);
				if (q == 1)
					return false;
				if (q == (n - 1)) {
					jePseudoslozen = false;
					break;
				}
			}
			if (jePseudoslozen == true)
				return false;
		}

	}
	return true;
}

int main() {
	int i, k = 0;
	for (i = 2;i <= 1000;i++) 
		if (jeProst(i) != jePseudoprost(i)) {
			printf("%d\n", i);
			k++;
		}
	printf("%d", k);
}