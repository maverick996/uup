/*
6. Napisati funkcije:
    double mojSin(double x, double eps)
    koja računa sumu x - x^3/3! + x^5/5! - x^7/7! ... sve dok je apsolutna vrednost člana veća ili jednaka od eps
    double mojCos(double x, double eps)
    koja računa sumu 1 - x^2/2! + x^4/4! - x^6/6! ... sve dok apsolutna vrednost člana veća ili jednaka od eps
    Sa ekrana se unosi pozitivno eps (neki mali broj npr. 0.0000001)
    generisati 10 slučajnih vrednosti x1,x2,..,x10 između -1 i 1 (isključujući -1 i 1) znači da |x| < 1 i izračunati apsolutnu vrednost razlika
    između mojSin(xi) i sin(xi) (sistemska funkcija za računanje sinusa iz <math.h>) kao i između mojCos(xi) i cos(xi) i na ekranu prikazati rezultate
 */

#include <stdio.h>
#include <math.h>
#include <stdlib.h>

const double eps = 1.0E-9;

double mojSin(double x, double eps) {
	double fakt, k, s, step, sum, sgn;
	fakt = sgn = k = 1;
	step = sum = x;
	while (fabs(sgn * step / fakt) >= eps) {
		sgn *= -1;
		fakt = fakt * (k + 1)*(k + 2);
		step = step * x * x;
		sum += sgn * step / fakt;
		k+=2;
	}
	return sum;
}
double mojCos(double x, double eps) {
	double fakt, k, s, step, sum, sgn;
	fakt = sgn = sum = step = 1;
	k = 0;
	while (fabs(sgn * step / fakt) >= eps) {
		sgn *= -1;
		fakt = fakt * (k + 1)*(k + 2);
		step = step * x * x;
		sum += sgn * step / fakt;
		k+=2;
	}
	return sum;
}


int main() {
	double n, r, rcos, rsin;
	int i;
	for (i = 1;i <= 10;i++) {
		n = (rand() % 10000) / 5000.0 - 0.9999;
		rsin = fabs(mojSin(n, eps) - sin(n));
		rcos = fabs(mojCos(n, eps) - cos(n));
		printf("x%d: % lf\n", i, n);
		printf("mojSin: % lf \t sin: % lf \t |mojSin - sin| = %.20lf\n",mojSin(n,eps), sin(n), rsin);
		printf("mojCos: % lf \t cos: % lf \t |mojCos - cos| = %.20lf\n\n",mojCos(n, eps), cos(n), rcos);
	}
}
