#include <stdio.h>
#include <math.h>

int main()
{
    int n;
    long long member, finalSum = 0;

    scanf("%d", &n);

        for (int i=n, j=1; i>0; i--, j++) {             //shorter solution
            member = pow(i, 2) + j;                     //finalSum += pow((pow(i,2) + j), 3);
            finalSum += pow(member, 3);
        }

    printf("%lld\n", finalSum);


    return 0;
}
