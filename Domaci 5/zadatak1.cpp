#include <stdio.h>
#include <math.h>

int main()
{
    double x, y, finalSum=0.0, numerator, denominator;
    int n, i=1;

    scanf("%lf%lf%d", &x, &y, &n);

        if (n%2==0)     //n - even
            do {                                            //shorter solution
                numerator = pow((x-i), i);                  //sum += pow((x-i), i) / pow((y+i), i);
                denominator = pow((y+i), i);
                finalSum += numerator / denominator;
                i++;
            } while (i<=n);
        else            //n - odd
            do {                                            //shorter solution
                numerator = pow((y-i), i);                  //sum += pow((y-i), i) / pow((x+i), i);
                denominator = pow((x+i), i);
                finalSum += numerator / denominator;
                i++;
            } while (i<=n);

    printf("%.8lf\n", finalSum);


    return 0;
}
