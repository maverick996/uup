#include <stdio.h>
#include <math.h>

int main()
{
    int n, memberDegree;
    double x, y, finalSum=0.0, memberValue;

    scanf("%lf%lf%d", &x, &y, &n);

        for (int i=1, j=3*n; i<=n; i++, j-=3) {                     // shorter solution
            memberValue = pow(x, i) + pow(y, j) + j - i;            // finalSum += pow((pow(x, i) + pow(y, j) + j - i), ((i+j)%2 + 1));
            memberDegree = (i+j)%2 + 1;
            finalSum += pow(memberValue, memberDegree);
        }

    printf("%.5lf\n", finalSum);


    return 0;
}
