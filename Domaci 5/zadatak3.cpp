#include <stdio.h>

int main()
{
    int p, q, r;
    long long n, product = 1, remainder;

    scanf("%d%d%d%lld", &p, &q, &r, &n);

        for (int i=0, j=0, k=0; ( (p>i) && (q>j) && (r>k)); i++, j+=2, k+=3)
            product*=(p-i + q-j + r-k);

        remainder = product % n;

    printf("%lld\n", remainder);


    return 0;
}
